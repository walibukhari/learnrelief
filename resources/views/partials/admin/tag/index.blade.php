@extends('layouts.admin')


@section('content')
    @include('partials.admin.tag.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Tag ID</th>
                        <th title="Field #2">Tag Name</th>
                        <th title="Field #3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($tags as $tag)
                            <tr>
                                <td scope="row">{{ $tag->id }}</td>
                                <td>{{ $tag->name }}</td>
                                <td>
                                    <form action="{{ route('tags.destroy', ['tag' => $tag]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('tags.show', ['tag' => $tag]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <!--end: Datatable -->
            </div>
        </div>
    </div>

@endsection