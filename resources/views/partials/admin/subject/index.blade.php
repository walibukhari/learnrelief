@extends('layouts.admin')


@section('content')
    @include('partials.admin.subject.contentheader')

    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <!--begin: Datatable -->
                <table width="100%" class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th title="Field #1">Subject ID</th>
                        <th title="Field #2">Subject Name</th>
                        <th title="Field #3">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($subjects as $subject)
                            <tr>
                                <td scope="row">{{ $subject->id }}</td>
                                <td>{{ $subject->name }}</td>
                                <td>
                                    <form action="{{ route('subjects.destroy', ['subject' => $subject]) }}" method="post" class="inline-block">
                                        <span><a href="{{ route('subjects.show', ['subject' => $subject]) }}" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">Edit</a></span>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" onclick="deleteFile(event, {{$subject}})" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row" style="font-size: 12px">
                    <div class="col-12 d-flex justify-content-center pt-3">
                        {{$subjects->links()}}
                    </div>
                </div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>

    <script>


        function deleteFile(event, subject) {

             if (subject.files.length && subject.quizzes.length)
            {
                let c = confirm("This subject has associations with one or many files which in turn may have associations with one or more quizzes. Are you sure your want to delete this subject. The corresponding quiz will also be delete permanently")
                if (!c){
                    event.preventDefault();
                }
            }
            else if (subject.files.length)
            {
                let c = confirm("This subject has associations with one or many files which in turn may have associations with one or more quizzes. Are you sure your want to delete this subject. It will also delete the corresponding files as well as detach them from corresponding quizzes")
                if (!c){
                    event.preventDefault();
                }

            }

            else {
                let c = confirm("Are you sure you want to delete this subject?")

                if(!c){
                    event.preventDefault();
                }
            }
        }
    </script>

@endsection