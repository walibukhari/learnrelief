@foreach($categories as $category)
    @foreach($category->subCategories as $subCategory)
        <section>
            <div class="container">
                @if($subCategory->jobsTests->count() > 0)
                    <div class="section-title mb-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <h2 class="mt-0 text-uppercase font-28 line-bottom line-height-1">Our <span class="text-theme-color-2 font-weight-400">{{ $subCategory->name }}</span></h2>
                                </div>
                            </div>
                    </div>
                @else

                @endif

                @if($subCategory->jobsTests->count() > 0)
                    @foreach($subCategory->jobsTests as $jobsTest)
                        <div class="section-content">
                            <div class="row multi-row-clearfix">
                                <div class="col-sm-6 col-md-3 sm-text-center mb-sm-30">
                                    <div class="team maxwidth400">
                                        <div class="thumb"><img class="img-fullwidth" src="{{ asset('storage/'.$jobsTest->image) }}" alt=""></div>
                                        <div class="content border-1px border-bottom-theme-color-2-2px p-15 bg-light clearfix">
                                            <h4 class="name text-theme-color-2 mt-0">{{ $jobsTest->name }}</h4>
                                                                                    <p class="mb-20">Lorem ipsum dolor sit amet, con amit sectetur adipisicing elit.</p>
                                                                                    <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-left flip">
                                                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                                    </ul>
                                            <a class="btn btn-dark btn-theme-colored btn-sm text-uppercase mt-10" href="page-courses-accounting-technologies.html">view courses</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else

                @endif
            </div>
        </section>
    @endforeach
@endforeach