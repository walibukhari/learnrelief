@extends('layouts.client')

@section('content')
    <div class="main-content">

        <!-- Section: home -->
        @include('partials.client._home.slider')

        <!-- Section: About -->
{{--        @include('partials.client._home.about')--}}

        <!-- Section: COURSES -->
{{--        @foreach($subCategories as $subCategory)--}}
{{--            @include('partials.client._home.courses')--}}
{{--        @endforeach--}}

        <!-- Divider: Funfact -->
{{--        @include('partials.client._home.funfacts')--}}

        <!-- Section: team -->

        <!-- Section: Gallery -->
        @include('partials.client._home.news')
        @include('partials.client._home.categories')

        @include('partials.client._home.subjects')

        <!-- Section: Why Choose Us -->
{{--        @include('partials.client._home.whychooseus')--}}

        <!-- Divider: testimonials -->
        @include('partials.client._home.banner')

        <!-- Section: blog -->
        @include('partials.client._home.blogs')

        <!-- Divider: Clients -->
{{--        @include('partials.client._home.clients')--}}


    <!-- end main-content -->
    </div>
@endsection