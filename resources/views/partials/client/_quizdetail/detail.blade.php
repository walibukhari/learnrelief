<div class="col-md-8 blog-pull-right">
    <div class="single-service">
        <img src="{{ $quiz->image }}" onerror="this.src='{{ asset('assets/images/logo.jpeg') }}'" alt="" class="w-75">
        <h3 class="line-bottom mt-20">{{ $quiz->name }}</h3>

        <h6>{{ $quiz->description }}</h6>

        <form class="mt-40">
            <h5>Select Mode</h5>
                <div>
                    <input type="radio" id="mode1" name="mode" value="0" checked>
                    <label for="mode1">Practice mode</label>
                </div>
                <div>
                    <input type="radio" id="mode2" name="mode" value="1">
                    <label  for="mode2">Test mode</label>
                </div>
                <input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
                <button type="button" onclick="goingToStartQuiz()" class="btn btn-primary customLoginStyle">Start Quiz</button>
        </form>
    </div>
</div>

<script>
    function goingToStartQuiz() {
        let mode1 =  document.getElementById("mode1").checked;
        let mode2 = document.getElementById("mode2").checked ;
        let quiz_name, quiz_mode, quiz_id;
        console.log('mode1,mode2');
        console.log(mode1);
        console.log(mode2);
        if (mode1) {
            quiz_mode = 0;
        }
        if (mode2) {
            quiz_mode = 1;
        }
        quiz_name = '{{str_replace(' ','-',$quiz->name)}}';
        quiz_id = '{{$quiz->id}}';

        let url = '/client/start/quiz/' + quiz_name + '/' + quiz_mode + '/' + quiz_id;
        console.log(url);
        window.location.href=url;
    }
</script>
