@extends('layouts.client')

@section('content')

    <!-- Section: Course -->
    <section>
      <div class="container">
        <div class="row">
            <!-- Course Detail -->
            @include('partials.client._quizdetail.detail')

            <!-- Left-side-bar -->
            @include('partials.client._quizdetail.leftsidebar')

        </div>
      </div>
    </section>
@endsection