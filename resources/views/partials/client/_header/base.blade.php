<header id="header" class="header">

    @include('partials.client._header.topbar')
    @include('partials.client._header.brand')
    @include('partials.client._header.navbar')
</header>