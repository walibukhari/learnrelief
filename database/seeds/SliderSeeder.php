<?php

use App\Slider;
use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::create([
            'primary_text' => 'Learn Relief',
            'secondary_text' => 'Online Courses',
            'image' => 's1.jpg'
        ]);

        Slider::create([
            'primary_text' => 'Easy Learning',
            'secondary_text' => 'Prepare tests online',
            'image' => 's2.jpg'

        ]);

        Slider::create([
            'primary_text' => 'Jobs Preparation',
            'secondary_text' => 'Prepare for appearing in jobs test',
            'image' => 's3.jpg'
        ]);
    }
}
