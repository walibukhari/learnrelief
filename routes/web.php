<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('layouts.client');
//});

//Auth::routes();
Route::post('login/user', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/confirm', 'Auth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('password/confirm', 'Auth\ConfirmPasswordController@confirm');

Route::get('/', 'ClientControllers\HomeController@index')->name('home');
//Route::get('/', function (){
//    return view('home',[
//        'subjects' => \App\Subject::all(),
//    ]);
//});
//Route::get('/about', 'ClientControllers\AboutController@index')->name('about');
//Route::get('/contact', 'ClientControllers\ContactController@index')->name('contact');
//Route::get('/jobs', 'ClientControllers\JobController@index')->name('jobs');
//Route::get('/tests', 'ClientControllers\AdmissionTestController@index')->name('tests');
//Route::get('/blogs', 'ClientControllers\BlogController@index')->name('blogs');
//Route::get('/blogs/detail', 'ClientControllers\BlogController@show')->name('blogs/detail');


