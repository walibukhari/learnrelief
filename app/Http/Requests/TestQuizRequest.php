<?php

namespace App\Http\Requests;

use App\Quiz;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TestQuizRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'description' => 'required',
        ];

        if (!$this->route('test_quiz'))
        {
            $rules += [
                'name' => 'required|max:100|'.Rule::unique('quizzes')
                        ->where('quizable_type', 'tests')
                        ->where('quizable_id', $this->quizable_id),
                'file' => 'required',
                'quizable_id' => 'required',
                'image' => 'required|mimes:jpg,png,jpeg'
            ];
        }
        else
        {
            $quiz = Quiz::find($this->route('test_quiz'));
            $rules += [
                'name' => 'required|max:100|'.Rule::unique('quizzes')
                        ->ignore($quiz->id)
                        ->where('quizable_type', 'tests')
                        ->where('quizable_id', $this->quizable_id),
                'file' => Rule::requiredIf(function () use($quiz) {
                    return $quiz->files->count() < 1;
                }),
                'quizable_id' => 'sometimes',
                'image' => 'sometimes|mimes:jpg,png,jpeg'
            ];
        }

        return $rules;
    }
}
