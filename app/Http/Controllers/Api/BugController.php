<?php

namespace App\Http\Controllers\Api;

use App\Contact;
use App\Events\BugReportedEvent;
use App\Http\Controllers\Controller;
use App\Mail\BugMail;
use App\Quiz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class BugController extends Controller
{
    public function report(Request $request)
    {
        $quiz = Quiz::find($request->quiz);

        if ($quiz)
        {
            $data = [
                'bug' => $request->bug,
                'quiz' => $quiz->name,
                'category' => $quiz->quizable->name,
                'question' => $request->question
            ];
//            $email = Contact::first()->email;
//            if ($email) {
            $bug = $request->bug;
            $name = $quiz->name;
            $category = $quiz->quizable->name;
            $question = $request->question;
            $this->sendBugReportEmail($bug,$name,$category,$question);
//                event(new BugReportedEvent($data));

            return response()->json([
                'status' => true,
                'message' => 'Bug reported successfully'
            ]);
//            }
//            return response()->json([
//                'status' => false,
//                'message' => 'Bug not reported'
//            ]);
        }

    }

    public function sendBugReportEmail($bug,$name,$category,$question)
    {
        $senderEmail = 'relieflearn@gmail.com';
        $senderName  ='Learn Relief';

        Mail::send('mails.sendBugReportEmail', array('bug' => $bug , 'name' => $name , 'category' => $category, 'question' => $question), function ($message) use ($senderEmail, $senderName) {
            $message->from($senderEmail, $senderName);
            $message->to('learnrel.team@gmail.com')
                ->subject('Bug Reported');
        });
    }
}
