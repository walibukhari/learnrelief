<?php

namespace App\Http\Controllers\ClientControllers;

use App\Http\Controllers\Controller;
use App\Quiz;
use App\Subject;
use Illuminate\Http\Request;

class AttemptController extends Controller
{
    public function index($name,$mode,$id)
    {
        $quiz = Quiz::find($id);
        return view('partials.client.quizpage.base', [
            'quiz' => $quiz,
            'mode' => $mode,
        ]);
    }
}
