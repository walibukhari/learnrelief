<?php

namespace App\Http\Controllers\ClientControllers;

use App\Blog;
use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function storeComment(Request $request, Blog $blog)
    {
        $comment = new Comment();
        $comment->createComment($blog);

        return back();
    }

    public function storeReply(Request $request, Blog $blog)
    {
        $comment = new Comment();
        $comment->createReply($blog);

        return back();
    }
}
