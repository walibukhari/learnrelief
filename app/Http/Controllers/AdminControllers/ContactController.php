<?php

namespace App\Http\Controllers\AdminControllers;

use App\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contact = Contact::first();
        return view('partials.admin.contact.index',[
            'contact' => $contact,
        ]);
    }

    public function update(Request $request, Contact $contact)
    {
        $data = $request->validate([
            'phone' => 'required|numeric',
            'address' => 'required',
            'email' => 'required|email'
        ]);
        $contact->update($data);

        return back();
    }
}
