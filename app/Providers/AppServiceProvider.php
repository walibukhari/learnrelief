<?php

namespace App\Providers;

use App\Banner;
use App\Blog;
use App\Category;
use App\ConciseRecord;
use App\Contact;
use App\Downloadable;
use App\File;
use App\Job;
use App\Observers\BlogObserver;
use App\Observers\FileObserver;
use App\Observers\JobObserver;
use App\Observers\SubjectObserver;
use App\Quiz;
use App\Slider;
use App\SubCategory;
use App\Subject;
use App\Tag;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use function PHPUnit\Framework\StaticAnalysis\HappyPath\AssertIsArray\consume;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Route::bind('admin/job-quizzes/{value}/{job_quiz}', function ($value) {
//            return Post::where('id', $value)
//                ->orWhere('name', $value)
//                ->first();
//        });

        Schema::defaultStringLength(191);

        Relation::morphMap([
            'subjects' => 'App\Subject',
            'jobs' => 'App\Job',
            'tests' => 'App\SubCategory',
            'downloadables' => 'App\Downloadable'
        ]);

//        dd(\Auth::guard('user')->user());
//        dd(2);
        Subject::observe(SubjectObserver::class);
        Job::observe(JobObserver::class);
        File::observe(FileObserver::class);
//        Blog::observe(BlogObserver::class);

        View::composer([
//            'partials.client._about.base',
//            'partials.client._tests.base',
//            'partials.client._blog.base',
//            'partials.client._blogdetail.base',
//            'partials.client._contact.base',
//            'partials.client.downloadables.index',
//            'partials.client._home.base',
//            'partials.client._joblist.base',
//            'partials.client._taggedblog.base',
//            'partials.client._joblist.base',
//            'partials.client._userupload.create',
//            'partials.client.profile.show',
//            'partials.client._quizlist.base',
//            'partials.client._jobsquiz.base',
//            'partials.client.courselist.base',
//            'partials.client._coursedetail.base',
//            'partials.client._quizdetail.base',
//            'partials.client.quizpage.base',
//            'partials.client._news.*',
//            'partials.client._newsdetail.*',
//            'partials.client._taggednews.base',
            'partials.client.*',
            'auth.*',

        ], function ($view){
            $view->with('tags', Tag::with('blogs')->get())
                    ->with('categories', Category::with(['subCategories.jobs'])->latest()->get())
                        ->with('downloadables', Downloadable::with('files')->get())
                            ->with('subjects', Subject::latest()->get())
                                ->with('banner', Banner::first())
                                    ->with('contact', Contact::first());
        });
    }
}
